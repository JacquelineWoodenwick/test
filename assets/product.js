// @DO - PDP-specific JS

// Update price on ATC; this function is called several places in theme.js
function updatePriceATC(qty, discount = 0, clipPrice = 0) {
 
  if (document.querySelector("[selected]")) {
    var price = document.querySelector("[selected]").innerHTML.split("- $")[1];
  } else {
    var price = document
      .querySelector("meta[property='product:price:amount']")
      .getAttribute("content");
  }
  if (price.includes(',')) {
    price = price.replace(',', '');
  }
  if (price.includes(' +')) {
    price = price.split(' +')[0];
  }
  price = Number(price) + clipPrice;
  var discountedAmount = price * (1 - discount);
  var total = discountedAmount * qty;
  total = total.toFixed(2);
  total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  if (document.querySelector("[data-money-convertible]")) {
    var target = document.querySelector("[data-money-convertible]");
  } else {
    var target = document.querySelector("#ATC-button-price");
  }
  target.outerHTML =
    "<span data-money-convertible>$" + total + "</span>";
}

// Update ATC price on page load - wicks PDP
window.addEventListener("load", () => {
  
  if (document.querySelector("#Quantity")) {
    updatePriceATC(document.querySelector("#Quantity").value, discount=0,window.currentClipPrice);
  }
});

// Switch PDP header image on thumbnail click

function changeProductHeaderImage(imgUrl) {
  document.querySelector('#Product__Hero').style.backgroundImage = "url("+imgUrl+")";
}

if (document.querySelector('.Product__SlideItem img')) {
  var thumbnailPDP = document.querySelectorAll('.Product__SlideItem img');
  for (var i = 0; i < thumbnailPDP.length; i++) {
    thumbnailPDP[i].addEventListener('click', function(ev){
      var imgUrl = '';
      if(ev.target.src.includes('.jpg')) {
        imgUrl = ev.target.src.split('_300x.jpg')[0] + '_1500x.jpg';
      } else {
        imgUrl = ev.target.src.split('_300x.png')[0] + '_1500x.png'; 
      }
      changeProductHeaderImage(imgUrl);
    })
  }
}

// Dynamically update Wick PDP on selection of Wick Type

// Move product info card to a different parent node on mobile
if (window.innerWidth < 1290) {
  window.addEventListener('load', function(){
    var card = document.querySelector('.bl-card');
    var newParent = document.querySelector('#mobile-info-box-container');
    try{
newParent.appendChild(card);
    }catch(error){

    }
    
  })
}

// Get product JSON
async function getProductData(handle) {
  var productData = await fetch('/products/' + encodeURI(handle) + '.json')
    .then(response => response.text())
    .then(data => { return data });

  var product = JSON.parse(productData).product;
  return product;
}

// Create buttons for additional variant selector items

function newOptButton(optSelector, optString, iteration) {
  var newWidthOpt = document.createElement("button");
  newWidthOpt.type = "button";
  newWidthOpt.classList.add('Popover__Value', 'Heading', 'Link', 'Link--primary', 'u-h6');
  if (iteration === 0) {
    newWidthOpt.classList.add('is-selected');
  }
  newWidthOpt.setAttribute('data-option-position', '1');
  newWidthOpt.innerHTML = optString;
  newWidthOpt.setAttribute('data-value', optString);
  optSelector.appendChild(newWidthOpt);
}

// Enable disabled form inputs

function enableInputs() {
  var formSelects = document.querySelectorAll('button.ProductForm__Item');
  var qty = document.querySelector('#Quantity');
  var atcButton = document.querySelector('.ProductForm__AddToCart');
  var dotSeparator = document.querySelector('.Button__SeparatorDot');
  var cartPrice = document.querySelector("[data-money-convertible]");

  dotSeparator.style.display = 'block';
  cartPrice.parentNode.style.display = 'block';

  function enabler(el) {
    el.disabled = false;
  }
  
  for (var i = 0; i < formSelects.length; i++) {
    enabler(formSelects[i]);
  }
  enabler(qty);
  enabler(atcButton);

}

// Update PDP DOM on SKU selection
if (document.querySelector('#wick-type-select-popup')) {
  window.addEventListener('load', function() {
    var wickSelectPopup = document.querySelector('#wick-type-select-popup');
    var wickSKUs = wickSelectPopup.children;
    for (var i = 0; i < wickSKUs.length; i++) {
      
      // Update info card.
      wickSKUs[i].addEventListener('mouseover', function(){
        var card = document.querySelector('.bl-card');
        card.style.display = 'block';
        var newCardHTML = document.querySelector('#' + this.dataset.value + '-info-box');
        card.innerHTML = newCardHTML.innerHTML;
      })
      
      // Grab product info and update the page.
      wickSKUs[i].addEventListener('click', function(event){
    
        document.querySelector('.Popover__Close').click();
        if (document.querySelector('.ProductForm__AddToCart').disabled) {
          enableInputs();
        }
        (async () => {
          var product = await getProductData(event.target.dataset.value);

          // --> Update DOM

          // Update a few standalone nodes
          document.querySelector('.ProductMeta__Title').innerHTML = product.title;
          document.querySelector('.ProductForm__SelectedValue').innerHTML = product.title;
          document.querySelector('.ProductMeta__Description .Rte').innerHTML = product.body_html;
          document.querySelector('#Product__Hero').style.backgroundImage = "url(" + product.image.src + ")";

          // Set a var to be checked when updating ATC price and applying discounts
          if (product.tags.includes('ready-made-custom-wick-t1')) {
            window.wickDiscountTag = 'ready_made_custom_wick_t1';
          } else if (product.tags.includes('ready-made-custom-wick-t2')) {
            window.wickDiscountTag = 'ready_made_custom_wick_t2';
          }

          // Update width selector
          var widthSelect = document.querySelector('.width-select-popover .Popover__Content .Popover__ValueList');
          document.querySelector('#width').innerHTML = product.variants[0].option1;
          widthSelect.innerHTML = '';
          var uniqueOpts = [];
          for (var i = 0; i < product.variants.length; i++) {
            var optString = product.variants[i].option1;
            if (!uniqueOpts.includes(optString)) {
              newOptButton(widthSelect, optString, i);
            }
            uniqueOpts.push(optString);
          }

          // Update height selector
          var heightSelect = document.querySelector('.height-select-popover .Popover__Content .Popover__ValueList');
          document.querySelector('#height').innerHTML = product.variants[0].option2;
          heightSelect.innerHTML = '';
          var uniqueOpts = [];
          for (var i = 0; i < product.variants.length; i++) {
            var optString = product.variants[i].option2;
            if (!uniqueOpts.includes(optString)) {
              newOptButton(heightSelect, optString, i);
            }
            uniqueOpts.push(optString);
          }

          // Update product image thumbnails
          var slides = document.querySelectorAll('.Product__SlideItem img');
          if (slides) {
            for (var i = 0; i < slides.length; i++) {
              slides[i].src = product.images[i].src;
            }
          }
         
          // Remove "pre-inserted" option on clip selector for tube & spiral wicks,
          // default back to "Separate"
          var preInsertOpt = document.querySelector('[data-value="Pre-Inserted"]');
          var preInsertSelected = document.querySelector('[data-clip-variant="Pre-Inserted"]');
          if (product.title.includes('Tube') || product.title.includes('Spiral')) {
            try{
              preInsertOpt.style.display = "none";
          }
          catch(error){}
            
            if (preInsertSelected) {
              preInsertSelected.innerHTML = "Separate";
              preInsertSelected.setAttribute('data-clip-variant', 'Separate');
            }
          } else {
          try{
              preInsertOpt.style.display = "block";
          }
          catch(error){}
           
          }
         
          // Update option selectors on option change
          var optionItems = document.querySelectorAll('.Popover__Value');
          for (var i = 0; i < optionItems.length; i++) {
            
            var itemParent = optionItems[i].parentNode;
            if (itemParent.id != ('wick-type-select')) {
            
              optionItems[i].addEventListener('click', function(event){
                               document.querySelector('body').click();
                var siblings = event.target.parentNode.children;
                for (var i = 0; i < siblings.length; i++) {
                  if (siblings[i].classList.contains('is-selected')) {
                    siblings[i].classList.remove('is-selected');
                  }
                }
                event.target.classList.add('is-selected');
                
                // reset itemParent and update selector
                itemParent = event.target.parentNode;
                if (itemParent.classList.contains('Width__List')) {
                  document.querySelector('#width').innerHTML = event.target.innerHTML;
                } else if (itemParent.classList.contains('Height__List')) {
                  document.querySelector('#height').innerHTML = event.target.innerHTML;
                }

                // Determine which clip is selected, then get that clip's price 
                // from the global vars set, on the wicks PDP - for use in calculating 
                // ATC price on update.
               
                var clipPriceVal = window.currentClipPrice;
                if (this.innerText == 'Pre-Inserted' || this.innerText == 'pre-inserted') {
                  clipPriceVal = window.flatClipPreinsertedPrice;
                } else if (this.innerText == 'Separate' || this.innerText == 'separate') {
                  clipPriceVal = window.flatClipSeparatePrice;
                } 
                
                if (this.innerText == 'None' || this.innerText == 'none') {
                  clipPriceVal = 0.00;
                }
            
                window.currentClipPrice = clipPriceVal;

                // update product variant ID based on option changes
                updateVariant(product, clipPriceVal);
               
              })
            }
          }

          updateVariant(product, window.currentClipPrice);

        })()
        
      })
    }
  })
}

// look up variant object by variant name and apply where needed
function updateVariant(product, clipPrice=0) {
  
  var prodWidth = document.querySelector('#width').innerHTML;
  var prodHeight = document.querySelector('#height').innerHTML;
  var title = prodWidth + ' / ' + prodHeight;
  var qty = document.querySelector("#Quantity").value;

  var obj = product.variants;
  var variantPrice = "0.00";

  for (var i = 0; i < obj.length; i++){
    if (obj[i].title == title){
      var variantID = obj[i].id;
      var variantSKU = obj[i].sku;
      var variantTitle = obj[i].title;
      if (obj[i].price) {
        variantPrice = obj[i].price;
      }
    }
  }

  // Update hidden variant selector
  var variantSelector = document.querySelector("[selected]");
  variantSelector.value = variantID;
  variantSelector.dataset.sku = variantSKU;
  variantSelector.innerHTML = variantTitle + " - $" + variantPrice;

  // Update ATC price
  updatePriceATC(qty,discount=window.discount,clipPrice);
  
  // Set global variables to be used by theme.js.
  window.currentVariant = {'id': variantID};
//  $('#frs_btn_none').click();
}

// Get discount amount; `discountObj` is a JSON string retrieved from
// the `discount-tiers` metafield
function getDiscountAmount(selector, qty, discountObj) {
  if (selector=='none') {
    return 0.00;
  }
  qty = Number(qty);
  var rounded_qty;
  var json = JSON.parse(discountObj);
  var tiers = json[selector].tiers;
  var tierQTYs = Object.keys(tiers);
  for ( var i = 0; i < tierQTYs.length; i++ ) {
    if ( qty >= tierQTYs[i] ) {
      rounded_qty = Number(tierQTYs[i]);
    }
  } 
  if (!json.hasOwnProperty(selector) || !json[selector].tiers.hasOwnProperty(rounded_qty)) {
    return 0.00;
  } else {
    return json[selector].tiers[rounded_qty].discount;
  }
}
